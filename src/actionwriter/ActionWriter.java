package actionwriter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.actionSystem.ex.ActionManagerEx;
import com.intellij.openapi.actionSystem.ex.AnActionListener;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ActionWriter implements ApplicationComponent {
    public static class State {
        public static final int curVersion = 1;
        public int version = curVersion;
        public String path = "./ActionTimestamps.txt";
        public String timestampFormat = "yyyy-MM-dd_hh:mm:ss:SSS";

        public static void SaveState(@NotNull State state) {
            PropertiesComponent pc = PropertiesComponent.getInstance();
            pc.saveFields(state);
        }

        public static State LoadState() {
            PropertiesComponent pc = PropertiesComponent.getInstance();
            State state = new State();
            int stateVersion = pc.getOrInitInt("version", -1);
            if(stateVersion==curVersion)
                pc.loadFields(state);
            return state;
        }
    }

    private static final Logger LOG = Logger.getInstance(ActionWriter.class);
    private static final String ACTION_WRITER_COMPONENT_NAME = "ActionWriter";

    private State _state = new State();
    private FileWriter _osw;

    private final AnActionListener anActionListener = new AnActionListener() {
        @Override
        public void beforeActionPerformed(AnAction anAction, DataContext dataContext, AnActionEvent anActionEvent) {
            synchronized (ActionWriter.this) {
                LOG.debug("beforeActionPerformed: " + anAction);
                try {
                    Calendar now = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(_state.timestampFormat);
                    String timeStamp = sdf.format(now.getTime());
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("timestamp", timeStamp);
                    Presentation presentation = anActionEvent.getPresentation();
                    JsonArray ja = new JsonArray();

                    for (Shortcut ss : anAction.getShortcutSet().getShortcuts()) {
                        ja.add(new JsonPrimitive(ss.toString()));
                    }
                    jsonObject.add("key", ja);
                    jsonObject.addProperty("action", presentation.getText());
                    final String jsonText = jsonObject.toString();
                    if (_osw != null) {
                        _osw.write(jsonText);
                        _osw.flush();
                    }
                    System.out.println(jsonText);
                } catch (IOException e) {
                    LOG.error(e);
                }
            }
        }

        @Override
        public void afterActionPerformed(AnAction anAction, DataContext dataContext, AnActionEvent anActionEvent) {

        }

        @Override
        public void beforeEditorTyping(char c, DataContext dataContext) {

        }
    };

    public ActionWriter() {
    }

    synchronized public void initComponent() {
        LOG.debug("initComponent");
        _state = State.LoadState();
        try {
            File file = new File(_state.path);
            System.out.println(file.getAbsolutePath());
            if (!file.exists()) {
                if (!file.createNewFile())
                    throw new IOException("Cant write to: " + file.toString());
            }
            _osw = new FileWriter(file, true);
        } catch (IOException e) {
            LOG.error(e);
            _osw = null;
        }
    }

    synchronized public void disposeComponent() {
        LOG.debug("disposeComponent");
        State.SaveState(_state);
        ActionManagerEx.getInstanceEx().removeAnActionListener(anActionListener);
        try {
            _osw.close();
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    @NotNull
    public String getComponentName() {
        return ACTION_WRITER_COMPONENT_NAME;
    }

    public void startListen() {
        LOG.debug("startListen");
        ActionManagerEx.getInstanceEx().removeAnActionListener(anActionListener);
        ActionManagerEx.getInstanceEx().addAnActionListener(anActionListener);
    }

    public void stopListen() {
        LOG.debug("stopListen");
        ActionManagerEx.getInstanceEx().removeAnActionListener(anActionListener);
    }

    public State getState() {
        return _state;
    }

    public void setState(State state) {
        _state = state;
    }
}
