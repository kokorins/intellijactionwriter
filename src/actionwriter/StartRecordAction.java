package actionwriter;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ex.ApplicationManagerEx;

public class StartRecordAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        ActionWriter actionWriter = ApplicationManagerEx.getApplicationEx().getComponent(ActionWriter.class);
        actionWriter.startListen();
    }
}
