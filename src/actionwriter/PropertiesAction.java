package actionwriter;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ex.ApplicationManagerEx;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PropertiesAction extends AnAction {
    private static final Logger LOG = Logger.getInstance(ActionWriter.class);

    static class PropertiesActionDialog extends DialogWrapper {
        private final JTextField _pathComponent;
        private final JTextField _timestampComponent;
        JPanel _central;

        public PropertiesActionDialog(actionwriter.ActionWriter.State state) {
            super(true);
            setTitle("Action Writer Properties");

            _central = new JPanel();

            final LayoutManager layout = new GridBagLayout();
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridx = 0;
            gbc.gridy = 0;
            _central.setLayout(layout);
            _central.add(new JLabel("Version:"), gbc);
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.gridwidth = 2;
            _central.add(new JLabel(Integer.toString(state.version)), gbc);

            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.gridwidth = 1;
            _central.add(new JLabel("Path"), gbc);
            gbc.gridx = 1;
            gbc.gridy = 1;
            gbc.gridwidth = 1;
            gbc.weightx = 2;
            _pathComponent = new JTextField(new File(state.path).getAbsolutePath());
            _central.add( _pathComponent, gbc);
            gbc.gridx = 2;
            gbc.gridy = 1;
            gbc.weightx = 1;
            JButton button = new JButton("Select");
            _central.add(button, gbc);
            button.addActionListener(e -> {
                JFileChooser jfc = new JFileChooser();
                jfc.setDialogTitle("Select log file");
                jfc.setMultiSelectionEnabled(false);
                jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                if (jfc.showOpenDialog(this.getContentPanel()) == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    try {
                        if (!selectedFile.exists()) {
                            boolean newFile = selectedFile.createNewFile();
                            if (!newFile)
                                throw new IOException("Cant create a log file");
                        }
                        _pathComponent.setText(selectedFile.getAbsolutePath());
                    } catch (IOException e1) {
                        LOG.error(e1);
                    }
                }
            });

            gbc.gridx = 0;
            gbc.gridy = 2;
            _central.add(new JLabel("Format:"), gbc);
            _timestampComponent = new JTextField(state.timestampFormat);
            gbc.gridwidth = 2;
            gbc.gridx = 1;
            gbc.gridy = 2;
            _central.add(_timestampComponent, gbc);
            init();
        }

        @NotNull
        protected Action[] createActions() {
            return new Action[]{getOKAction(), getCancelAction()};
        }


        protected JComponent createCenterPanel() {
            return _central;
        }

        public ActionWriter.State getState() {
            ActionWriter.State state = new ActionWriter.State();
            state.path = _pathComponent.getText();
            state.timestampFormat = _timestampComponent.getText();
            return state;
        }
    }

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        ActionWriter sw = ApplicationManagerEx.getApplicationEx().getComponent(ActionWriter.class);
        PropertiesActionDialog dialog = new PropertiesActionDialog(sw.getState());
        dialog.show();
        if (dialog.isOK()) {
            sw.setState(dialog.getState());
        }
    }
}
